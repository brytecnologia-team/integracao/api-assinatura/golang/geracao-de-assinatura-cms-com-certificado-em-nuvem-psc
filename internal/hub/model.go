package hub

type hubData struct {
	KmsData             kmsData  `json:"kms_data"`
	AlgoritmoHash       string   `json:"algoritmoHash"`
	FormatoAssinatura   string   `json:"formatoAssinatura"`
	FormatoDadosEntrada string   `json:"formatoDadosEntrada"`
	Padrao              string   `json:"padrao"`
	Perfil              string   `json:"perfil"`
	Hashes              []string `json:"hashes"`
	Comentarios         []string `json:"comentarios,omitempty"`
	NomeDocumentos      []string `json:"nomeDocumentos,omitempty"`
}

type kmsData struct {
	Url   string `json:"url"`
	Token string `json:"token"`
}

func newHubData(pscUrl, pscToken, hash, algoritmoHash, formatoAssinatura, padrao, perfil string, comentario, nomeDocumento []string) hubData {
	return hubData{
		KmsData: kmsData{
			Url:   pscUrl,
			Token: pscToken,
		},
		AlgoritmoHash:       algoritmoHash,
		FormatoAssinatura:   formatoAssinatura,
		FormatoDadosEntrada: "Base64",
		Padrao:              padrao,
		Perfil:              perfil,
		Hashes:              []string{hash},
		Comentarios:         comentario,
		NomeDocumentos:      nomeDocumento,
	}

}

type hubResponse struct {
	Assinaturas []string `json:"assinaturas"`
}

type tokenResponse struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   int64  `json:"expires_in"`
}
