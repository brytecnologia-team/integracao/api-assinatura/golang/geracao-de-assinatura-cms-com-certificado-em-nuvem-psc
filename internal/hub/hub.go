package hub

import (
	"bytes"
	"cms/psc/config"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
)

type hub struct {
	uri string
	tkn *token

	s config.Signature
}

func New(c config.Hub, s config.Signature) (Hub, error) {
	tkn, err := newToken(c.CloudUrl, c.ClientID, c.ClientSecret)
	if err != nil {
		return nil, err
	}

	return hub{
		uri: c.Url,
		tkn: tkn,
		s:   s,
	}, nil
}

func (h hub) CmsSignature(document []byte, pscUrl, pscToken string) (signedDocument []byte, err error) {
	// URL para acesso a API
	uri := h.uri + "/fw/v1/cms/kms/assinaturas"

	// Valor do documento. Para HASH, deve ser enviado o hash do documento como Base64, de acordo com o algoritmo escohido.
	// Para ATTACHED e DETACHED, deve ser enviado o documento como Base64.
	var documentBase64 string
	if h.s.FormatoAssinatura == "HASH" {
		var hash []byte
		switch h.s.AlgoritmoHash {
		case "SHA256":
			documentHash := sha256.Sum256(document)
			hash = documentHash[:]
		case "SHA512":
			documentHash := sha512.Sum512(document)
			hash = documentHash[:]
		default:
			return nil, fmt.Errorf("algoritmo de hash '%s' inválido", h.s.FormatoAssinatura)
		}
		documentBase64 = base64.StdEncoding.EncodeToString(hash)
	} else {
		documentBase64 = base64.StdEncoding.EncodeToString(document)
	}

	// Parâmetros não obrigatórios
	var comentarios []string
	if h.s.Comentario != "" {
		comentarios = []string{h.s.Comentario}
	}
	var nomeDocumentos []string
	if h.s.NomeDocumento != "" {
		nomeDocumentos = []string{h.s.NomeDocumento}
	}

	// Corpo da requisição. Para envios em lote, enviar múltiplos valores para hash, comentários e nomeDocumentos
	body := newHubData(pscUrl, pscToken, documentBase64, h.s.AlgoritmoHash, h.s.FormatoAssinatura, h.s.Padrao, h.s.Perfil, comentarios, nomeDocumentos)
	bodyBytes, err := json.Marshal(&body)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, uri, bytes.NewBuffer(bodyBytes))
	if err != nil {
		return nil, err
	}
	// Pega token JWT utilizado para requisição
	accessToken, err := h.tkn.getToken()
	if err != nil {
		return nil, err
	}
	req.Header.Set("kms_type", "PSC")
	req.Header.Set("Authorization", accessToken)
	req.Header.Set("Content-Type", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.Body == nil {
		return nil, fmt.Errorf("hub response body returned nil and status %d", resp.StatusCode)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		b, err := io.ReadAll(resp.Body)
		if err != nil {
			return nil, fmt.Errorf("error reading hub response body: %s", err.Error())
		}
		return nil, errors.New(string(b))
	}

	var response hubResponse
	err = json.NewDecoder(resp.Body).Decode(&response)
	if err != nil {
		return nil, err
	}

	if len(response.Assinaturas) <= 0 {
		return nil, errors.New("invalid hub response length")
	}
	// Para assinaturas em lote, utilizar array de assinaturas
	signedDocument, err = base64.StdEncoding.DecodeString(response.Assinaturas[0])
	if err != nil {
		return nil, fmt.Errorf("error formatting base64 of response: %s", err.Error())
	}
	return signedDocument, nil
}
