package hub

type Hub interface {
	CmsSignature(document []byte, pscUri, pscToken string) (signedDocument []byte, err error)
}
