package psc

import (
	"cms/psc/config"
	"cms/psc/internal/memory"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type psc struct {
	url          string
	redirectUrl  string
	clientId     string
	clientSecret string
	scope        string
	memory       memory.Memory
}

func New(c config.Psc, mem memory.Memory) Psc {
	return psc{
		url:          c.Url,
		redirectUrl:  c.RedirectUrl,
		clientId:     c.ClientID,
		clientSecret: c.ClientSecret,
		scope:        c.Scope,
		memory:       mem,
	}
}

// AccessCodeUri Gera URI de acesso a serviço PSC
func (p psc) AccessCodeUri(id, state string) (string, error) {
	// Gera um novo code challenge para requisição, armazenando Code Verifier na memória para a requisição de geração de token
	codeChallenge, err := p.newCodeChallenge(id)
	if err != nil {
		return "", err
	}

	// Query parameters necessários
	query := url.Values{}
	query.Set("response_type", "code")
	query.Set("code_challenge_method", "S256")
	query.Set("client_id", p.clientId)
	query.Set("scope", p.scope)
	query.Set("redirect_uri", p.redirectUrl)
	query.Set("state", state)
	query.Set("code_challenge", codeChallenge)
	queryStr := query.Encode()

	return fmt.Sprintf("%s/v0/oauth/authorize?%s", p.url, queryStr), nil
}

// AccessToken Gera token de acesso para o serviço PSC
func (p psc) AccessToken(id, code string) (accessToken string, err error) {
	uri := fmt.Sprintf("%s/v0/oauth/token", p.url)

	// Recupera código de verificação da memória
	anyCodeVerifier, ok := p.memory.Get(id)
	if !ok {
		return "", errors.New("error code verifier")
	}
	codeVerifier := anyCodeVerifier.(string)

	data := url.Values{}
	data.Set("grant_type", "authorization_code")
	data.Set("client_id", p.clientId)
	data.Set("client_secret", p.clientSecret)
	data.Set("redirect_uri", p.redirectUrl)
	data.Set("code", code)
	data.Set("code_verifier", codeVerifier)

	r, err := http.NewRequest(http.MethodPost, uri, strings.NewReader(data.Encode()))
	if err != nil {
		return "", err
	}
	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := http.DefaultClient.Do(r)
	if err != nil {
		return "", err
	}

	if resp.StatusCode != http.StatusOK {
		return "", errors.New(resp.Status)
	}

	var accessTokenRes struct {
		AccessToken string `json:"access_token"`
	}
	err = json.NewDecoder(resp.Body).Decode(&accessTokenRes)
	if err != nil {
		return "", err
	}
	return accessTokenRes.AccessToken, nil
}

func (p psc) GetHost() string {
	return p.url
}

func (p psc) newCodeChallenge(id string) (string, error) {
	// Gera um novo valor como CodeVerifier
	r := rand.New(rand.NewSource(time.Now().Unix()))
	randomBytes := make([]byte, 32)
	_, err := r.Read(randomBytes)
	if err != nil {
		return "", err
	}
	codeVerifier := base64.RawURLEncoding.EncodeToString(randomBytes)

	// Salva valor na memória para geração de token de acesso
	p.memory.Save(id, codeVerifier)

	// Calcula e retorna o valor de CodeChallenge
	codeChallengeBytes := sha256.Sum256([]byte(codeVerifier))
	codeChallenge := base64.RawURLEncoding.EncodeToString(codeChallengeBytes[:])

	return codeChallenge, nil
}
