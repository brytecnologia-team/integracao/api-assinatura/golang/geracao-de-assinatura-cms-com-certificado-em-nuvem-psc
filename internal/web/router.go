package web

import (
	"cms/psc/internal/hub"
	"cms/psc/internal/psc"
	"fmt"
	"net/http"
)

func Start(port int, h hub.Hub, p psc.Psc) error {
	handler := webHandler{
		Hub: h,
		Psc: p,
	}

	http.HandleFunc("/", handler.Form)
	http.HandleFunc("/image", handler.Image)

	http.HandleFunc("/code", handler.Code) // Redireciona para URL do serviço PSC, com os parâmetros necessários para geração de código de acesso;
	http.HandleFunc("/psc", handler.Token) // Tratamento para geração de token. Url de tratamento após autenticação do usuário no serviço PSC;
	http.HandleFunc("/sign", handler.Sign) // Realiza assinatura na API utilizando token de acesso;

	return http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
}
