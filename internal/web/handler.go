package web

import (
	"cms/psc/internal/hub"
	"cms/psc/internal/psc"
	_ "embed"
	"html/template"
	"io"
	"log"
	"net/http"
)

//go:embed static/form.html
var form []byte

//go:embed static/logo.png
var image []byte

//go:embed static/tokenDisplay.html
var token string
var tokenTemplate *template.Template

func init() {
	var err error
	tokenTemplate, err = template.New("tokenDisplay").Parse(token)
	if err != nil {
		panic(err)
	}
}

type webHandler struct {
	Hub hub.Hub
	Psc psc.Psc
}

func (h *webHandler) Form(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	_, _ = w.Write(form)
}

func (h *webHandler) Image(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Content-Type", "image/png")
	_, _ = w.Write(image)
}

func (h *webHandler) Code(w http.ResponseWriter, r *http.Request) {
	log.Println("Received request for code")

	url, err := h.Psc.AccessCodeUri(GetID(r), GetState(r))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, url, http.StatusFound)
}

func (h *webHandler) Token(w http.ResponseWriter, r *http.Request) {
	log.Println("Received request for token")

	id := GetID(r)
	errorMsg := r.URL.Query().Get("error")
	if errorMsg != "" {
		http.Error(w, errorMsg, http.StatusBadRequest)
		return
	}
	code := r.URL.Query().Get("code")

	tkn, err := h.Psc.AccessToken(id, code)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	_ = tokenTemplate.Execute(w, tkn)
}

func (h *webHandler) Sign(w http.ResponseWriter, r *http.Request) {
	log.Println("Received request for Signature")

	err := r.ParseMultipartForm(32 << 20)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if r.MultipartForm == nil || r.MultipartForm.File == nil {
		http.Error(w, "multipart form or path is required", http.StatusInternalServerError)
		return
	}
	fhs := r.MultipartForm.File["documento"]
	if len(fhs) <= 0 {
		http.Error(w, "multipart form files is required", http.StatusInternalServerError)
		return
	}

	document, err := fhs[0].Open()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer document.Close()

	documentBytes, err := io.ReadAll(document)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	tkn := r.MultipartForm.Value["token"][0]
	if tkn == "" {
		http.Error(w, "token is required", http.StatusInternalServerError)
		return
	}

	signature, err := h.Hub.CmsSignature(documentBytes, h.Psc.GetHost(), tkn)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/octet-stream")
	w.Header().Set("Content-Disposition", "attachment; filename=\"signed.p7s\"")
	_, _ = w.Write(signature)
}

func GetID(r *http.Request) string {
	return "singleUser"
}

func GetState(r *http.Request) string {
	return "singleState"
}
