package memory

type Memory interface {
	Save(id string, value any)
	Get(id string) (any, bool)
}
