package config

type Psc struct {
	Url          string
	RedirectUrl  string
	ClientID     string
	ClientSecret string
	Scope        string
}

type Hub struct {
	Url          string
	CloudUrl     string
	ClientID     string
	ClientSecret string
}

type Signature struct {
	AlgoritmoHash     string
	FormatoAssinatura string
	Padrao            string
	Perfil            string
	Comentario        string
	NomeDocumento     string
}

var Config = struct {
	Port      int
	Hub       Hub
	Psc       Psc
	Signature Signature
}{
	Port: 8000,

	Signature: Signature{
		AlgoritmoHash:     "SHA512",                                     // Algoritmo de hash utilizado na assinatura. SHA256, SHA512
		FormatoAssinatura: "HASH",                                       // Formato da assinatura. HASH, ATTACHED, DETACHED
		Padrao:            "CMS",                                        // Padrão da assinatura. CMS, CADES
		Perfil:            "TIMESTAMP",                                  // Perfil da assinatura. BASIC, TIMESTAMP, COMPLETE, ADRB, ADRT, ADRV, ADRC, ADRA, ETSI_B, ETSI_T, ETSI_LT e ETSI_LTA
		Comentario:        "Assinatura de exemplo com certificado PSC.", // Comentários adicionados a assinatura
		NomeDocumento:     "",                                           // Nome dos documentos de assinatura
	},

	Hub: Hub{
		Url:          "<HUB_URL>",   // Ex: Produção: https://hub2.bry.com.br, Homologação: https://hub2.hom.bry.com.br
		CloudUrl:     "<CLOUD_URL>", // Ex: Produção: https://cloud.bry.com.br, Homologação: https://cloud-hom.bry.com.br
		ClientID:     "<CLOUD_CLIENT_ID>",
		ClientSecret: "<CLOUD_CLIENT_SECRET>",
	},

	Psc: Psc{
		Url:          "<PSC_URL>",      // Verifique com seu provedor de acesso ao certificado em nuvem sua URL base
		RedirectUrl:  "<REDIRECT_URL>", // Ex: http://localhost:8000/psc
		ClientID:     "<PSC_CLIENT_ID>",
		ClientSecret: "<PSC_CLIENT_SECRET>",
		Scope:        "signature_session", // Valores disponíveis: single_signature | multi_signature | signature_session
	},
}
