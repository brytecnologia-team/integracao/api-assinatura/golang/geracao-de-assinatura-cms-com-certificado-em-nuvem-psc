# Assinatura CMS com Certificado em Nuvem

Código de exemplo de integração dos serviços da API de assinatura com clientes baseados em tecnologia Golang para assinatura CMS com certificado em nuvem PSC.

Este exemplo gera um token de acesso de acordo com [RFC 6749](https://datatracker.ietf.org/doc/html/rfc6749) e o utiliza para realizar assinatura CMS; 

### Variáveis que devem ser configuradas

O exemplo necessita ser configurado com credenciais de acesso válidas. É necessário configurar credenciais de acesso para API de assinatura e credenciais de acesso para serviço de Certificado em Nuvem.

As credenciais de acesso a API de assinatura podem ser obtidas através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastra-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente à produção da assinatura.

| Variável              | Descrição                                                                      | Local da Configuração |
|-----------------------|--------------------------------------------------------------------------------|-----------------------|
| <HUB_URL>             | Url base para API de assinatura.                                               | /config/config.go     |
| <CLOUD_URL>           | Url base para API de geração de tokens de autenticação para API de assinatura. | /config/config.go     |
| <CLOUD_CLIENT_ID>     | Client ID para geração de token JWT da API de assinatura.                      | /config/config.go     |
| <CLOUD_CLIENT_SECRET> | Client Secret para geração de token JWT da API de assinatura.                  | /config/config.go     |

| Variável            | Descrição                                                                                                                               | Local da Configuração |
|---------------------|-----------------------------------------------------------------------------------------------------------------------------------------|-----------------------|
| <PSC_URL>           | Url base para acesso ao PSC.                                                                                                            | /config/config.go     |
| <REDIRECT_URL>      | Url de redirecionamento utilizada pelo PSC. O exemplo realiza tratamento de redirecionamentos na rota `/psc`                            | /config/config.go     |
| <PSC_CLIENT_ID>     | Client ID para autenticação no PSC.                                                                                                     | /config/config.go     |
| <PSC_CLIENT_SECRET> | Client Secret para autenticação no PSC.                                                                                                 | /config/config.go     |
| <PSC_SCOPE>         | Escopo para assinaturas realizadas utilizando token obtido no PSC. Para este exemplo, utilize `single_signature` ou `signature_session` | /config/config.go     |

### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência, preencha as variáveis de autenticação e execute o programa:
```
go run main.go
```

A página de exemplo de assinatura estará disponível através da porta configurada.

Para realizar a assinatura CMS, é necessário autorizar o uso do certificado gerando um token de acesso, utilizando botão `Gerar Token`