package main

import (
	"cms/psc/config"
	"cms/psc/internal/hub"
	"cms/psc/internal/memory"
	"cms/psc/internal/psc"
	"cms/psc/internal/web"
	"log"
)

func main() {
	log.Println("Initializing http service")
	c := config.Config
	mem := memory.New()
	p := psc.New(c.Psc, mem)
	h, err := hub.New(c.Hub, c.Signature)
	if err != nil {
		panic(err)
	}

	err = web.Start(c.Port, h, p)
	log.Println("Error on web service: ", err)
}
